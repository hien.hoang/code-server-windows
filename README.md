# Code Server Windows

## 1. Start the virtual machine

```sh
vagrant up
```

## 2. Run the code server

```sh
vagrant ssh

cd coder
sudo docker-compose up -d
```

## 3. Copy the password

After everything is up, there will be a folder `.config` appear. Open the file `.config/code-server/config.yaml` 

```yaml
bind-addr: 0.0.0.0:8080
auth: password
password: 3******************5
cert: false
```

## 4. Open the Browser at `http://localhost:8080`

Use the password and enjoy
